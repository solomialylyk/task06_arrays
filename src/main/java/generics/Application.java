package generics;

import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        Product ph = new Phone("samsung", 8000, "3425");
        print(ph);
        Product cam = new Camera ("ni", 36000, 1000);
        print(cam);
        Container<Camera> c;
        List<Camera> cameras = new ArrayList<Camera>();
        find(cameras, new Camera());
        find(cameras, new Phone());

        find2(cameras, new Camera());
       // find2(cameras, new Phone());
        List<Phone> phones = new ArrayList<Phone>();
        find2(phones, new Phone());

        PrirityQue prior= new PrirityQue();
        prior.added(new Pair("MyString", 3));
        prior.added(new Pair(1, 1));
        prior.added(new Pair(2, 2));
        System.out.println(prior);



    }
    static boolean find(List<? extends Product> all, Product p) {
        return true;
    }
    static<T extends Product>boolean find2(List<T> all, T p) {
        return true;
    }
    static<T> void print(T product) {
        System.out.println(product);
    }
}
