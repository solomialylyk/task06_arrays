package generics;

public class Pair<T>{
    T value;
    int priority;

    public Pair(T value, int priority) {
        this.value = value;
        this.priority = priority;
    }


    @Override
    public String toString() {
        return "Pair{" +
                "value=" + value +
                ", priority=" + priority +
                '}';
    }
}
