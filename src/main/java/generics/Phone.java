package generics;

public class Phone extends Product{
    String model;

    public Phone(String name, int price, String model) {
        super(name, price);
        this.model = model;
    }

    public Phone() {

    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "model='" + model + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
