package generics;

public class Camera extends Product implements Comparable<Camera>{
    int pixels;

    public Camera(String name, int price, int pixels) {
        super(name, price);
        this.pixels = pixels;
    }

    public Camera() {
        super();
    }

    @Override
    public String toString() {
        return "Camera{" +
                "pixels=" + pixels +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    public int getPixels() {
        return pixels;
    }

    public void setPixels(int pixels) {
        this.pixels = pixels;
    }
    @Override
    public int compareTo(Camera o) {
        return 0;
    }
}

